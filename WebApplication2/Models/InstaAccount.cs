﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Specialized;
using System.Net;

namespace WebApplication2.Models
{
    public class InstaAccount
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string ClientUri { get; set; }
        public string Code { get; set; }
        public string AccessToken { get; set; }

        public InstaAccount(string clientId, string clientSecret, string clientUri, string code)
        {
            this.ClientId = clientId;
            this.ClientSecret = clientSecret;
            this.ClientUri = clientUri;
            this.Code = code;
            AccountHelper.CurrentAccount = this;
        }

        public object GenerateAccessToken()
        {
            try
            {
                NameValueCollection parameters = new NameValueCollection();
                parameters.Add("client_id", AccountHelper.CurrentAccount.ClientId);
                parameters.Add("client_secret", AccountHelper.CurrentAccount.ClientSecret);
                parameters.Add("grant_type", "authorization_code");
                parameters.Add("redirect_uri", AccountHelper.CurrentAccount.ClientUri);
                parameters.Add("code", AccountHelper.CurrentAccount.Code);

                WebClient client = new WebClient();
                var result = client.UploadValues("https://api.instagram.com/oauth/access_token", "POST", parameters);
                var token = JsonConvert.DeserializeObject(System.Text.Encoding.Default.GetString(result));

                return token;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}