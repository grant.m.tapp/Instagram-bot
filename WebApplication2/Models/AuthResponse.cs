﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication2.Models
{
    public class GenericResponse
    {
        public bool Successful { get; set; }
        public string Error { get; set; }
    }

    public class AuthResponse 
    {
        public string ClientId => ConfigurationManager.AppSettings["instagram.clientid"];
        public string ResponseUri => ConfigurationManager.AppSettings["instagram.redirecturl"];
        public string ClientSecret => ConfigurationManager.AppSettings["instagram.clientsecret"];
    }
}