﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class AccountHelper
    {
        public static InstaAccount CurrentAccount { get; set; }
        public static string AccessToken { get; set; }
        public static string ClientId => ConfigurationManager.AppSettings["instagram.clientid"];
        public static string ClientSecret => ConfigurationManager.AppSettings["instagram.clientsecret"];
        public static string ClientUri => ConfigurationManager.AppSettings["instagram.redirecturl"];
        public static string Code => HttpContext.Current.Request["code"];
    }
}