﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            new InstaAccount(AccountHelper.ClientId, AccountHelper.ClientSecret, AccountHelper.ClientUri, AccountHelper.Code);
            
            return View();
        }

        public JsonResult GetCurrentAccount()
        {
            return Json(AccountHelper.CurrentAccount);
        }

        public object GetAccessToken()
        {
            return AccountHelper.CurrentAccount.GenerateAccessToken();
        }
    }
}
