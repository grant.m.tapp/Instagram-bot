﻿function webMethod(url, type, headers, body, success) {
    $.ajax({
        async: false,
        url: url,
        type: type,
        headers: headers,
        body: body,
        success: success
    })
}

function generateAccessToken(id, uri, secret, code) {
    $.ajax({
        async: false,
        url: "/Home/GetAccessToken",
        type: "POST",
        success: function (data)
        {
            console.log(data);
        }
    })
}

function generateCode() {
    $.ajax({
        async: false,
        url: "/Home/GetCurrentAccount",
        type: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        success: function (data) {
            window.location.href = `https://api.instagram.com/oauth/authorize/?client_id=${data.ClientId}&redirect_uri=${data.ClientUri}&response_type=code`;
        }
    })
}